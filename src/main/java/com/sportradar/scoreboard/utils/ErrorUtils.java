package com.sportradar.scoreboard.utils;

public final class ErrorUtils {
    public static final String GAME_ALREADY_STARTED = "Game %s - %s already started!";
    public static final String GAME_NOT_EXISTS = "Game %s - %s does not exist!";
    public static final String DOWNGRADE_SCORE = "Can't update the score %d:%d with values below the current!";
    public static final String TEAM_NAME_EMPTY = "Team names can't be empty! Home team: %s, Away team: %s";

    private ErrorUtils() {
    }
}
