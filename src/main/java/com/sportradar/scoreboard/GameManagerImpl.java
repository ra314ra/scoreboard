package com.sportradar.scoreboard;

import com.sportradar.scoreboard.Game.Scores;
import com.sportradar.scoreboard.Game.Teams;
import com.sportradar.scoreboard.utils.ErrorUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class GameManagerImpl implements GameManager {
    private final Map<Teams, Scores> games = new HashMap<>();

    @Override
    public void startGame(String homeTeam, String awayTeam) {
        validateTeamEmpty(homeTeam, awayTeam);

        Teams teams = new Teams(homeTeam, awayTeam);
        if (games.containsKey(teams)) {
            String message = String.format(ErrorUtils.GAME_ALREADY_STARTED, homeTeam, awayTeam);
            throw new IllegalArgumentException(message);
        }

        games.put(teams, new Scores());
    }

    @Override
    public void endGame(String homeTeam, String awayTeam) {
        validateTeamEmpty(homeTeam, awayTeam);

        Teams teams = new Teams(homeTeam, awayTeam);
        if (!games.containsKey(teams)) {
            String message = String.format(ErrorUtils.GAME_NOT_EXISTS, homeTeam, awayTeam);
            throw new IllegalArgumentException(message);
        }

        games.remove(teams);
    }

    @Override
    public void updateScore(String homeTeam, String awayTeam, int homeScore, int awayScore) {
        validateTeamEmpty(homeTeam, awayTeam);

        Teams teams = new Teams(homeTeam, awayTeam);

        if (!games.containsKey(teams)) {
            String message = String.format(ErrorUtils.GAME_NOT_EXISTS, homeTeam, awayTeam);
            throw new IllegalArgumentException(message);
        }

        games.computeIfPresent(teams, (team, scores) -> {
            int existingHomeScore = scores.getHomeScore();
            int existingAwayScore = scores.getAwayScore();
            if (homeScore < existingHomeScore || awayScore < existingAwayScore) {
                String message = String.format(ErrorUtils.DOWNGRADE_SCORE, existingHomeScore, existingAwayScore);
                throw new IllegalArgumentException(message);
            }
            scores.setHomeScore(homeScore);
            scores.setAwayScore(awayScore);
            return scores;
        });
    }

    private static void validateTeamEmpty(String homeTeam, String awayTeam) {
        if (homeTeam == null || awayTeam == null || homeTeam.isEmpty() || awayTeam.isEmpty()) {
            String message = String.format(ErrorUtils.TEAM_NAME_EMPTY, homeTeam, awayTeam);
            throw new IllegalArgumentException(message);
        }
    }

    @Override
    public Summary getGamesSummary() {
        List<Game> gameList = games.entrySet().stream()
                .sorted(Map.Entry.comparingByValue())
                .map(entry -> new Game(entry.getKey(), entry.getValue()))
                .collect(Collectors.toList());

        return new Summary(gameList);
    }
}
