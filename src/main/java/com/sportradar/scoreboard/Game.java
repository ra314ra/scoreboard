package com.sportradar.scoreboard;

import java.time.LocalDateTime;
import java.util.Objects;

public class Game {
    private final Teams teams;
    private final Scores scores;

    public Game(Teams teams, Scores scores) {
        this.teams = teams;
        this.scores =scores;
    }

    public String getHomeTeam() {
        return teams.getHomeTeam();
    }

    public String getAwayTeam() {
        return teams.getAwayTeam();
    }

    public int getHomeScore() {
        return scores.getHomeScore();
    }
    public int getAwayScore() {
        return scores.awayScore;
    }

    public static class Teams {
        private final String homeTeam;
        private final String awayTeam;

        public Teams(String homeTeam, String awayTeam) {
            this.homeTeam = homeTeam;
            this.awayTeam = awayTeam;
        }

        public String getHomeTeam() {
            return homeTeam;
        }

        public String getAwayTeam() {
            return awayTeam;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Teams otherTeams = (Teams) o;
            return Objects.equals(homeTeam, otherTeams.homeTeam) && Objects.equals(awayTeam, otherTeams.awayTeam);
        }

        @Override
        public int hashCode() {
            return Objects.hash(homeTeam, awayTeam);
        }
    }

    public static class Scores implements Comparable<Scores> {
        private int homeScore;
        private int awayScore;
        private final LocalDateTime startTime;


        public Scores() {
            this.homeScore = 0;
            this.awayScore = 0;
            this.startTime = LocalDateTime.now();
        }

        public int getHomeScore() {
            return homeScore;
        }

        public int getAwayScore() {
            return awayScore;
        }

        public void setHomeScore(int homeScore) {
            this.homeScore = homeScore;
        }

        public void setAwayScore(int awayScore) {
            this.awayScore = awayScore;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Scores scores = (Scores) o;
            return homeScore == scores.homeScore && awayScore == scores.awayScore && Objects.equals(startTime, scores.startTime);
        }

        @Override
        public int hashCode() {
            return Objects.hash(homeScore, awayScore, startTime);
        }

        @Override
        public int compareTo(Scores that) {
            int thisScore = this.homeScore + this.awayScore;
            int thatScore = that.homeScore + that.awayScore;

            if (thisScore == thatScore) {
                return this.startTime.compareTo(that.startTime);
            }

            return thisScore < thatScore ? 1 : -1;
        }
    }
}
