package com.sportradar.scoreboard;

public interface GameManager {
    void startGame(String homeTeam, String awayTeam);
    void endGame(String homeTeam, String awayTeam);
    void updateScore(String homeTeam, String awayTeam, int homeScore, int awayScore);
    Summary getGamesSummary();
}
