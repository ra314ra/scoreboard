package com.sportradar.scoreboard;

import java.util.List;

public class Summary {
    private final List<Game> games;

    public Summary(List<Game> games) {
        this.games = games;
    }

    public List<Game> getGames() {
        return games;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Game game : games) {
            sb.append(game.getHomeTeam());
            sb.append(" ");
            sb.append(game.getHomeScore());
            sb.append(" - ");
            sb.append(game.getAwayScore());
            sb.append(" ");
            sb.append(game.getAwayTeam());
            sb.append("%n");
        }
        return sb.toString();
    }
}
