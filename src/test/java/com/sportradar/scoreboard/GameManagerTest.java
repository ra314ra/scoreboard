package com.sportradar.scoreboard;

import com.sportradar.scoreboard.utils.ErrorUtils;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class GameManagerTest {

    @Test
    void startGame() {
        String homeTeam = "Spain";
        String awayTeam = "Portugal";
        GameManager gameManager = new GameManagerImpl();
        gameManager.startGame(homeTeam, awayTeam);

        Summary summary = gameManager.getGamesSummary();
        List<Game> games = summary.getGames();
        assertFalse(games.isEmpty());
        assertEquals(1, games.size());
    }

    @Test
    void startGameWithNullsFails() {
        String homeTeam = null;
        String awayTeam = null;
        GameManager gameManager = new GameManagerImpl();

        String errorMessage = String.format(ErrorUtils.TEAM_NAME_EMPTY, homeTeam, awayTeam);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                gameManager.startGame(homeTeam, awayTeam));
        assertEquals(errorMessage, exception.getMessage());
    }

    @Test
    void startGameAlreadyInProgressFail() {
        String homeTeam = "Spain";
        String awayTeam = "Portugal";
        GameManager gameManager = new GameManagerImpl();
        gameManager.startGame(homeTeam, awayTeam);

        String errorMessage = String.format(ErrorUtils.GAME_ALREADY_STARTED, homeTeam, awayTeam);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                gameManager.startGame(homeTeam, awayTeam));
        assertEquals(errorMessage, exception.getMessage());
    }

    @Test
    void endGame() {
        String homeTeam = "Spain";
        String awayTeam = "Portugal";

        GameManager gameManager = new GameManagerImpl();
        gameManager.startGame(homeTeam, awayTeam);
        gameManager.endGame(homeTeam, awayTeam);

        Summary summary = gameManager.getGamesSummary();
        List<Game> games = summary.getGames();
        assertTrue(games.isEmpty());
    }

    @Test
    void endGameNonExistentFail() {
        String homeTeam = "Spain";
        String awayTeam = "Portugal";

        GameManager gameManager = new GameManagerImpl();
        gameManager.startGame(homeTeam, awayTeam);
        gameManager.endGame(homeTeam, awayTeam);

        String errorMessage = String.format(ErrorUtils.GAME_NOT_EXISTS, homeTeam, awayTeam);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                gameManager.endGame(homeTeam, awayTeam));
        assertEquals(errorMessage, exception.getMessage());
    }

    @Test
    void endGameNullFail() {
        String homeTeam = null;
        String awayTeam = null;

        GameManager gameManager = new GameManagerImpl();

        String errorMessage = String.format(ErrorUtils.TEAM_NAME_EMPTY, homeTeam, awayTeam);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                gameManager.endGame(homeTeam, awayTeam));
        assertEquals(errorMessage, exception.getMessage());
    }

    @Test
    void endGameEmptyFail() {
        String homeTeam = "";
        String awayTeam = "";

        GameManager gameManager = new GameManagerImpl();

        String errorMessage = String.format(ErrorUtils.TEAM_NAME_EMPTY, homeTeam, awayTeam);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                gameManager.endGame(homeTeam, awayTeam));
        assertEquals(errorMessage, exception.getMessage());
    }

    @Test
    void updateScore() {
        String homeTeam = "Spain";
        String awayTeam = "Portugal";

        int homeScore = 1;
        int awayScore = 0;

        GameManager gameManager = new GameManagerImpl();
        gameManager.startGame(homeTeam, awayTeam);
        gameManager.updateScore(homeTeam, awayTeam, homeScore, awayScore);

        Summary summary = gameManager.getGamesSummary();
        List<Game> games = summary.getGames();
        assertEquals(1, games.size());
        Game game = games.get(0);
        assertEquals(homeTeam, game.getHomeTeam());
        assertEquals(awayTeam, game.getAwayTeam());
        assertEquals(homeScore, game.getHomeScore());
        assertEquals(awayScore, game.getAwayScore());
    }

    @Test
    void updateScoreOfNonEndedGameFail() {
        String homeTeam = "Spain";
        String awayTeam = "Portugal";

        int homeScore = 1;
        int awayScore = 0;

        GameManager gameManager = new GameManagerImpl();
        gameManager.startGame(homeTeam, awayTeam);
        gameManager.updateScore(homeTeam, awayTeam, homeScore, awayScore);
        gameManager.endGame(homeTeam, awayTeam);

        String errorMessage = String.format(ErrorUtils.GAME_NOT_EXISTS, homeTeam, awayTeam);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                gameManager.updateScore(homeTeam, awayTeam, homeScore, awayScore));
        assertEquals(errorMessage, exception.getMessage());
    }

    @Test
    void updateScoreOfNonExistentGameFail() {
        String homeTeam = "Spain";
        String awayTeam = "Portugal";
        String wrongTeam = "Brazil";

        int homeScore = 1;
        int awayScore = 0;

        GameManager gameManager = new GameManagerImpl();
        gameManager.startGame(homeTeam, awayTeam);
        gameManager.updateScore(homeTeam, awayTeam, homeScore, awayScore);

        String errorMessage = String.format(ErrorUtils.GAME_NOT_EXISTS, homeTeam, wrongTeam);
        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                gameManager.updateScore(homeTeam, wrongTeam, homeScore, awayScore));
        assertEquals(errorMessage, exception.getMessage());
    }

    @Test
    void updateScoreDownScoreFail() {
        String homeTeam = "Spain";
        String awayTeam = "Portugal";

        int homeScore = 1;
        int awayScore = 0;

        GameManager gameManager = new GameManagerImpl();
        gameManager.startGame(homeTeam, awayTeam);
        gameManager.updateScore(homeTeam, awayTeam, homeScore, awayScore);

        String errorMessage = String.format(ErrorUtils.DOWNGRADE_SCORE, homeScore, awayScore);

        Exception exception = assertThrows(IllegalArgumentException.class, () ->
                gameManager.updateScore(homeTeam, awayTeam, 0, 0));
        assertEquals(errorMessage, exception.getMessage());
    }

    @Test
    void getGamesSummaryOrderChangesOnScoreUpdated() {
        String homeTeam1 = "Spain";
        String awayTeam1 = "Portugal";

        int homeScore1 = 1;
        int awayScore1 = 0;

        String homeTeam2 = "Uruguay";
        String awayTeam2 = "Brazil";

        int homeScore2 = 1;
        int awayScore2 = 0;

        GameManager gameManager = new GameManagerImpl();

        gameManager.startGame(homeTeam1, awayTeam1);
        gameManager.updateScore(homeTeam1, awayTeam1, homeScore1, awayScore1);

        gameManager.startGame(homeTeam2, awayTeam2);
        gameManager.updateScore(homeTeam2, awayTeam2, homeScore2, awayScore2);

        Summary summary = gameManager.getGamesSummary();
        List<Game> games = summary.getGames();
        assertEquals(2, games.size());

        Game gameFirst = games.get(0);
        assertEquals(homeTeam1, gameFirst.getHomeTeam());
        assertEquals(awayTeam1, gameFirst.getAwayTeam());
        assertEquals(homeScore1, gameFirst.getHomeScore());
        assertEquals(awayScore1, gameFirst.getAwayScore());

        Game gameSecond = games.get(1);
        assertEquals(homeTeam2, gameSecond.getHomeTeam());
        assertEquals(awayTeam2, gameSecond.getAwayTeam());
        assertEquals(homeScore2, gameSecond.getHomeScore());
        assertEquals(awayScore2, gameSecond.getAwayScore());

        gameManager.updateScore(homeTeam2, awayTeam2, 2, 1);

        summary = gameManager.getGamesSummary();
        games = summary.getGames();
        assertEquals(2, games.size());

        gameFirst = games.get(0);
        assertEquals(homeTeam2, gameFirst.getHomeTeam());
        assertEquals(awayTeam2, gameFirst.getAwayTeam());
        assertEquals(2, gameFirst.getHomeScore());
        assertEquals(1, gameFirst.getAwayScore());

        gameSecond = games.get(1);
        assertEquals(homeTeam1, gameSecond.getHomeTeam());
        assertEquals(awayTeam1, gameSecond.getAwayTeam());
        assertEquals(homeScore1, gameSecond.getHomeScore());
        assertEquals(awayScore1, gameSecond.getAwayScore());
    }
}